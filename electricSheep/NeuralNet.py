# Todo:
#  Add bias's
#  Add regularization
#  new cost functions
#  handle multiple outputs - auto for nOutputClass > 2
#  Genertic class for ML lib and data management (partioning, CV)
#  Stochastic or bacth sizes                                                                        
#  Maybe...implement (maybe hessian netwons L-BFGS)

import numpy as np


class NeuralNet:

    # Constants for gradient checking
    GRAD_CHECK_THRESH = 10e-8
    GRAD_CHECK_ITER = 50
    EPSILSON = 10e-6

    def __init__(self, maxIter=500, nHidden=3):
        # Set hyperparameters
        self.maxIter = maxIter
        self.nHidden = nHidden
        self.learningRate = np.linspace(0.5, 0.01, self.maxIter)
        self.momentum = 0.5
        self.enNesterov = True  # Nesterov accelerated gradient (https://arxiv.org/pdf/1212.0901v2.pdf)
        self.cost = []
        self.gradDiff = []
        self.minGrad = []
        self.dW1 = 0
        self.dW2 = 0

    """
    initNet
    Initialise network structure and weights
    """
    def initNet(self, X, y):
        self.X = np.hstack((np.ones((X.shape[0], 1), dtype=X.dtype), X)) # Add bias
        self.y = y
        self.nInput = self.X.shape[1]
        self.nOutput = self.y.shape[1]
        self.W1 = np.random.rand(self.nInput, self.nHidden)/100
        self.W2 = np.random.rand(self.nHidden+1, self.nOutput)/100

    """
    train
    Train network by minimizing cost through backpropagation
    """
    def train(self, _X, _y):

        # Initialise network structure
        self.initNet(_X, _y)

        # Train - full batch
        for m in range(self.maxIter):

            # Train net
            yHat = self.feedforward(self.X)
            J = self.costFunction(self.y, yHat)
            self.cost.append(J)
            dJdW1, dJdW2 = self.backprop(self.X, self.y, yHat)
            self.updateWeights(dJdW1, dJdW2, self.learningRate[m])

            # Check gradient descent is operating as expected
            if not m % self.GRAD_CHECK_ITER:
                if not (self.checkGradients()):
                    print('[FAIL] Gradient checking. diff = {0}'.format(self.gradDiff[-1]))

    """
    feedforward
    Propagate input features forwards through the network
    """
    def feedforward(self, X):
        # First layer
        self.z2 = np.dot(X, self.W1)
        self.a2 = self.sigmoid(self.z2)
        # Second layer
        a2 = np.hstack((np.ones((self.a2.shape[0], 1), dtype=self.a2.dtype), self.a2))  # Add bias
        self.z3 = np.dot(a2, self.W2)
        yHat = self.sigmoid(self.z3)
        return yHat

    """
    costFunction
    Calculate global cost for current weights
    """
    def costFunction(self, y, yHat):
        J = 0.5 * np.sum((y - yHat)**2)
        return J

    """
    backprop
    Perform backpropagation of errors to calculate gradients
    """
    def backprop(self, X, y, yHat):
        # NB: delta 3 depends on the cost function applied
        delta3 = np.multiply(-(y - yHat), self.sigmoidPrime(self.z3))
        a2 = np.hstack((np.ones((self.a2.shape[0], 1), dtype=self.a2.dtype), self.a2))  # Add bias
        dJdW2 = np.dot(a2.T, delta3)
        delta2 = np.dot(delta3, self.W2[1:].T) * self.sigmoidPrime(self.z2)
        dJdW1 = np.dot(X.T, delta2)
        return dJdW1, dJdW2

    """
    updateWeights
    Update the weights based on the gradient of the errors
    """
    def updateWeights(self, dJdW1, dJdW2, learnRate):
        if self.enNesterov:
            dW1_prev = self.dW1
            dW2_prev = self.dW2
            self.dW1 = learnRate*dJdW1 + self.momentum*self.dW1
            self.dW2 = learnRate*dJdW2 + self.momentum*self.dW2
            self.W1 = self.W1 - (1+self.momentum)*self.dW1 - self.momentum*dW1_prev
            self.W2 = self.W2 - (1+self.momentum)*self.dW2 - self.momentum*dW2_prev
        else:
            self.dW1 = learnRate*dJdW1 + self.momentum*self.dW1
            self.dW2 = learnRate*dJdW2 + self.momentum*self.dW2
            self.W1 = self.W1 - self.dW1
            self.W2 = self.W2 - self.dW2

    """
    getWeights
    Gets unrolled vector of weights
    """
    def getWeights(self):
        return np.concatenate((self.W1.ravel(), self.W2.ravel()))

    """
    setWeights
    Sets weights according to network structure from unrolled vectors
    """
    def setWeights(self, weights):
        W1_end = self.nInput * self.nHidden
        self.W1 = np.reshape(weights[0:W1_end], (self.nInput, self.nHidden))
        W2_end = W1_end + self.nHidden * self.nOutput + 1
        self.W2 = np.reshape(weights[W1_end:W2_end], (self.nHidden+1, self.nOutput))

    """
    checkGradients
    Compare backpropagation gradients to numerically estimated gradients
    """
    def checkGradients(self):
        # Numerical gradient
        numGrad = self.computeNumericalGradient()

        # Backprop gradient
        yHat = self.feedforward(self.X)
        dJdW1, dJdW2 = self.backprop(self.X, self.y, yHat)
        grad = np.concatenate((dJdW1.ravel(), dJdW2.ravel()))
        self.minGrad.append(np.min(grad))

        # Compare
        diff = np.linalg.norm(grad - numGrad) / np.linalg.norm(grad + numGrad)
        self.gradDiff.append(diff)
        if diff < self.GRAD_CHECK_THRESH:
            return True
        else:
            return False

    """
    computeNumericalGradient
    Use finite difference approximation to numerically estimate gradient
    """
    def computeNumericalGradient(self):
        weights = self.getWeights()
        perturb = np.zeros(weights.shape)
        numGrad = np.zeros(weights.shape)

        for p in range(len(weights)):
            # Set pertubation for this weight only
            perturb[p] = self.EPSILSON
            # Positive perturbation
            self.setWeights(weights + perturb)
            yHat = self.feedforward(self.X)
            Jpos = self.costFunction(self.y, yHat)
            # Negative perturbation
            self.setWeights(weights - perturb)
            yHat = self.feedforward(self.X)
            Jneg = self.costFunction(self.y, yHat)
            # Compute Numerical Gradient
            numGrad[p] = (Jpos - Jneg) / (2 * self.EPSILSON)
            # Reset perturbation for next iteration
            perturb[p] = 0

        # Reset weights
        self.setWeights(weights)
        return numGrad

    """
    sigmoid
    Activation function
    """
    @staticmethod
    def sigmoid(z):
        return 1 / (1 + np.exp(-z))

    """
    sigmoidPrime
    Derivative of activation function
    """
    @staticmethod
    def sigmoidPrime(z):
        x = np.exp(-z)
        return x / ((1 + x)**2)
