"""
NOTE THIS IS WORK IN PROGESS!

genetic.py
Graeme Lyon

Simple class for genetic algorithm learning.
Takes in bounds for each weight parameter and generates a population of
chromosones. The chromosones are evaluated and the fittest chroms are evolved
through combination and mutation. The amount of mutation is gradually decreased
to perform simulated annealing in order to improve the probability of locating
the global minima of the error manifold.

Input Parameters
n_pop - number of chromosones in population
gen_gap - percentage of new population allowed in next generation
mut_rate - probability a chrom will be mutated
mut_shrink - max factor we allow mutation by - simulated annealing
field_lims - max/min limits placed on each weight parameter

ToDo:
Vectorization can be improved
Single function to check field limit bounds
Remove Chrom class as use numpy arrays thoughout?
"""

import numpy as np
import math


class Genetic:

    def __init__(self, n_pop, gen_gap, mut_rate, mut_shrink, field_lims):
        if n_pop % 2 != 0:
            n_pop += 1
        self.n_pop = n_pop
        self.gen_gap = gen_gap
        self.gen_switch = round(gen_gap * n_pop)
        self.mut_rate = mut_rate
        self.mut_shrink = mut_shrink
        self.n_params = len(field_lims[:, 0])
        self.field_lims = field_lims
        self.population = []
        self.n_gen = 0

    """
    createPopulation
    Create a population of randomized weights within the configured bounds (field_lims)
    """
    def createPopulation(self):
        N = self.n_pop
        # Generate weights withing min and max bounds for each parameter
        temp = np.subtract(self.field_lims[:, 1], self.field_lims[:, 0])
        span = np.repeat(temp.reshape(1, self.n_params), N, 0)
        temp = self.field_lims[:, 0]
        lower = np.repeat(temp.reshape(1, self.n_params), N, 0)
        temp = np.multiply(np.random.rand(N, self.n_params), span)
        chrom_array = np.add(temp, lower)
        # Create population
        self.population = [Chrom(wts) for wts in chrom_array]

    """
    calcFitness
    Convert the raw objective value into relative fitness for each chrom
    """
    def calcFitness(self):
        # todo: can optimize this if we use numpy arrays throughout
        sumObjV = 0
        for c in self.population:
            sumObjV += c.getObjVal()
        # get relative fitness of chroms (for minimiziation only!)
        ftness_sum = 0
        for c in self.population:
            ftness = 1 / (c.getObjVal() / sumObjV)
            ftness_sum += ftness
            c.setFitness(ftness)
        for c in self.population:
            c.setFitness(c.getFitness() / ftness_sum)

    """
    rouletteSelect
    Select new population using relative fitness as probability
    """
    def rouletteSelect(self):
        # Generate probability intervals for each individual
        probs = [i.getFitness() for i in self.population]
        new_population = []
        for n in range(self.n_pop):
            idx = self.select(probs)
            new_population.append(self.population[idx])
        self.population[:self.gen_switch] = new_population[:self.gen_switch]

    """
    select
    Helper function for roulette selection process
    """
    def select(self, probs):
        r = np.random.random()
        i = 0
        while r > 0:
            r = r - probs[i]
            i += 1
        return i - 1

    """
    crossOver
    Cross over pairs of chroms to introduce evolution to the population
    """
    def crossOver(self):
        Xops = math.floor(self.n_pop / 2)
        rnge = 1
        alpha1 = -rnge + ((1 + (2 * rnge)) * np.random.random([Xops, self.n_params]))
        alpha2 = -rnge + ((1 + (2 * rnge)) * np.random.random([Xops, self.n_params]))
        data = [i.getWeights() for i in self.population]
        chrom_array = np.array(data)
        new_chrom_array = chrom_array
        # x-over chroms using S1 = alpha*(S1-S2)
        diffs = chrom_array[::2] - chrom_array[1::2]
        new_chrom_array[1::2] = chrom_array[1::2] + np.multiply(alpha1, diffs)
        new_chrom_array[::2] = chrom_array[::2] + np.multiply(alpha2, diffs)
        # Check for out of bounds
        mins = np.repeat(self.field_lims[:, 0].reshape(1, self.n_params), self.n_pop, 0)
        maxes = np.repeat(self.field_lims[:, 1].reshape(1, self.n_params), self.n_pop, 0)
        new_chrom_array = np.maximum(new_chrom_array, mins)
        new_chrom_array = np.minimum(new_chrom_array, maxes)
        # Rebuild population
        self.population = [Chrom(wts) for wts in new_chrom_array]

    """
    mutate
    Randomly mutate selected chroms based on mut_rate
    """
    def mutate(self):
        for c in self.population:
            r = np.random.random()
            if r < self.mut_rate:
                # mutate this chrom
                wts = c.getWeights()
                # select which weight(s) to mutate
                i = round(np.random.random() * (self.n_params - 1))
                # mutate weight(s)
                pol = 1 if (np.random.random() < 0.5) else -1
                wts[i] += wts[i] * pol * self.mut_shrink[self.n_gen]
                # Check for out of bounds
                wts[i] = max(wts[i], self.field_lims[i, 0])
                wts[i] = min(wts[i], self.field_lims[i, 1])
                c.setWeights(wts)
        self.n_gen += 1

    """
    getMinObjVal
    Get minimum objective value
    """
    def getMinObjVal(self):
        self.minObjV = np.inf
        for individual in self.population:
            if individual.getObjVal() < self.minObjV:
                self.minObjV = individual.getObjVal()
        return self.minObjV

    """
    getMinObjValWeights
    Get current best set of weights
    """
    def getMinObjValWeights(self):
        for individual in self.population:
            if individual.getObjVal() == self.minObjV:
                return individual.getWeights()

    """
    addChrom
    Manually add a chrom back into the population
    """
    def addChrom(self, wts):
        for individual in self.population:
            individual.setWeights(wts)
            break


class Chrom(object):
    """
    Chrom
    Holder object for each chromosone and their data
    """
    def __init__(self, weights):
        self.weights = weights

    def setWeights(self, wts):
        self.weights = wts

    def getWeights(self):
        return self.weights

    def setObjVal(self, objV):
        self.objV = objV

    def getObjVal(self):
        return self.objV

    def setFitness(self, fitness):
        self.fitness = fitness

    def getFitness(self):
        return self.fitness

# EOF
