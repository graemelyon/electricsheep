#!/usr/bin/python

"""
test_genetic.py
Graeme Lyon

The goal of this test script is simply to validate the genetic algorithm module
in its ability to map a set of input features to non-linear target function.
In this case the target function is:
y = x + 3*cos(x)

The weights are initialized randomly within set bounds and the algorithm
performs optimization through evolution of the parameter space.
estimate = wt0*f1 + wt1*f2, where f1 = x and f2 = cos(x)
The error between the non-linear target and estimate model is minimized
and the results are plotted after 300 generations.
"""

import numpy as np
import sys
import pandas as pd
import matplotlib.pyplot as plt
import math
from electricSheep import Genetic


PLOT = True


def main():

    # Load data (uses pandas for convenience because that is what the project uses)
    df = pd.read_csv('../datasets/genetic_test_data.csv', index_col='example',
                     names=['example', 'f1', 'f2', 'target'])
    # Shuffle data
    df = df.sample(frac=1).reset_index(drop=True)
    df_train = df.ix[0:100]
    df_test = df.ix[101:199]
    df_xval = df.ix[200:299]
    # Split to train, x-val and test
    train_features = df_train.values[:, 0:-1]
    train_target = df_train.values[:, -1]
    test_features = df_test.values[:, 0:-1]
    test_target = df_test.values[:, -1]
    xval_features = df_xval.values[:, 0:-1]
    xval_target = df_xval.values[:, -1]

    # Genetic.py algorithm parameters
    max_gen = 300
    n_pop = 100
    gen_gap = 0.75
    mut_rate = 0.30
    mut_shrink = np.linspace(0.1, 0.0001, num=max_gen)
    field_lims = np.array([[-50, 50], [-50, 50]])  # Define weight boundaries
    # Create
    gen = Genetic(n_pop, gen_gap, mut_rate, mut_shrink, field_lims)
    gen.createPopulation()

    # Begin evolution of chroms
    for g in range(max_gen):
        print('Generation ' + str(gen.n_gen))
        # Evaluate each chrom on training dataset
        for chrom in gen.population:
            train_err = check_performance(chrom.getWeights(), train_features, train_target)
            chrom.setObjVal(train_err)

        # Calculate error for this generation
        gen.getMinObjVal()
        best_wts = gen.getMinObjValWeights()
        print(best_wts)
        train_err = check_performance(best_wts, train_features, train_target)
        xval_err = check_performance(best_wts, xval_features, xval_target)

        if PLOT and (gen.n_gen % 4) == 0:
            if gen.n_gen == 0:
                plt.figure(0)
                plt.ion()
                plt.ylabel('Cost')
                plt.xlabel('N generation')
                text = plt.text(0.1, 0.8, "", verticalalignment='center',
                                transform=plt.gca().transAxes, fontsize=13)
            plt.semilogy(gen.n_gen, train_err, 'xb', label='train_error')
            plt.semilogy(gen.n_gen, xval_err, 'xr', label='xval_error')
            text.set_text("Cost\nTrain = {:0.5f}\nX-val = {:0.5f}".format(train_err, xval_err))
            plt.draw()
            plt.pause(0.01)

        # Evolve our population!
        # Evaluate, select, combine and mutate chroms
        gen.calcFitness()
        gen.rouletteSelect()
        gen.crossOver()
        gen.mutate()
        gen.addChrom(best_wts)

    # Test system performance
    h_test = np.dot(test_features, best_wts)
    stats_test = get_stats(test_target, h_test)

    # Plot some results
    if PLOT:
        plt.ioff()
        fig = plt.figure(1)
        ax1 = fig.add_subplot(111)
        ax1.plot(df['f1'], df['target'], 'xr', label='target function')
        ax1.plot(df_test['f1'].values, h_test, 'ob', label='model estimate')
        plt.text(0.8, 0.1, "RMSD = {:0.2f}\nbias = {:0.2f}\nstd = {:0.2f}".format(stats_test['rmsd'],
                 stats_test['bias'], stats_test['std']), verticalalignment='center',
                 transform=ax1.transAxes, fontsize=13)
        plt.legend(loc="upper left")
        plt.show()


def check_performance(wts, f, target):
    h = np.dot(f, wts)
    return sum((target - h)**2) / len(h)


def get_stats(y, h):
    difs = y - h
    std = np.std(difs)
    bias = np.mean(difs)
    stats = {'rmsd': math.sqrt(bias**2 + std**2), 'bias': bias, 'std': std}
    return stats


if __name__ == "__main__":
    main()
    print('Complete!\n')
    sys.exit

# EOF
