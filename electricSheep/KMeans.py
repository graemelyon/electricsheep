import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

class KMeans:

    def __init__(self, k=4, maxIter=500, tol=1e-6):
        self.nCentroids = k
        self.maxIter = maxIter
        self.centroids = np.empty(self.nCentroids)
        self.color = ['r', 'g', 'b', 'm', 'c', 'k']
        self.tol = tol

    """
    seedCentroids
    Sets initial centroids using the first k examples
    """
    def seedCentroids(self, X):
        self.centroids = X[0:self.nCentroids]
        self.prevCentroids = np.zeros(self.centroids.shape)

    """
    train
    Identify clusters
    """
    def train(self, X):
        self.seedCentroids(X)
        mExamples = X.shape[0]
        n = 0

        while n < self.maxIter and not self.solutionConverged():

            # Find closest centroid for each example
            labels = np.empty(mExamples)
            for m in range(mExamples):
                dist = self.euclidianDistance(X[m, :], self.centroids)
                labels[m] = np.argmin(dist)

            # Update centroids
            self.prevCentroids = np.copy(self.centroids)
            plt.cla()
            plt.ion()
            for c in range(self.nCentroids):
                # Update current centroid
                self.centroids[c, :] = X[labels == c, :].mean(axis=0)

                # Plotting
                plt.scatter(X[labels == c, 0], X[labels == c, 1], c=self.color[c], s=5)
                plt.plot(self.centroids[c, 0], self.centroids[c, 1], c=self.color[c],
                         marker="o", markersize=12, markeredgewidth=2, markeredgecolor='k')
                plt.title('m = {0}'.format(n))
            plt.pause(0.1)

            n += 1
        plt.show(block=True)

    """
    solutionConverged
    Checks if centroids have remained stable since last iteratio
    """
    def solutionConverged(self):
        if (np.abs(self.prevCentroids - self.centroids) < self.tol).all():
            print('Solution converged.')
            return True
        else:
            return False

    """
    euclidianDistance
    Calculates euclidian distance for a single training example
    """
    @staticmethod
    def euclidianDistance(x, centroids):
        return np.sum((x - centroids)**2, axis=1)

