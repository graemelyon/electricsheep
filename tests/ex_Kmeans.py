#!/usr/bin/python

import sys
import numpy as np
from electricSheep import KMeans


def main():

    kClusters = 3

    # Generate some test data
    mExamples = 1000
    mFeatures = 2      # Plotting only supports 2 dimensions
    X = np.random.randn(mExamples, mFeatures)
    # Split into equal groupings and add mean offsets
    sz = round(X.shape[0] / kClusters)
    for k in range(kClusters):
        X[k*sz:, :] = X[k*sz:, :] + k*2
        X[:, k*sz:] = X[:, k*sz:] - k**2
    np.random.shuffle(X)

    KM = KMeans(k=kClusters)
    KM.train(X)


if __name__ == "__main__":
    main()
    print('Complete!\n')
    sys.exit()