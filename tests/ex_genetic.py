#!/usr/bin/python

import numpy as np
import sys
import pandas as pd
import matplotlib.pyplot as plt
import math
from electricSheep import Genetic



def main():

    # Load data into pandas dataframe
    df = pd.read_csv('../datasets/genetic_test_data.csv', index_col='example',
                     names=['example', 'f1', 'f2', 'target'])